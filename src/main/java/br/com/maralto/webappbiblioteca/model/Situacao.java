package br.com.maralto.webappbiblioteca.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.maralto.webappbiblioteca.util.model.PersistentEntity;

@Entity
@Table(name = "SITUACOES")
@AttributeOverride(name = "id", column = @Column(name = "SITUACOES_ID"))
public class Situacao extends PersistentEntity {

	private static final long serialVersionUID = 1L;
	@Column(name = "SITUACOES_DESCRICAO")
	private String descricao;
	
	public Situacao() {
	}

	public Situacao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
