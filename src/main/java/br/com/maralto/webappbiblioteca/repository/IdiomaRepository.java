package br.com.maralto.webappbiblioteca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.maralto.webappbiblioteca.model.Idioma;


public interface IdiomaRepository extends JpaRepository<Idioma, Long>{
	

}
