package br.com.maralto.webappbiblioteca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.maralto.webappbiblioteca.model.Situacao;


public interface SituacaoRepository extends JpaRepository<Situacao, Long>{
	

}
