package br.com.maralto.webappbiblioteca.service;

import java.util.List;

import br.com.maralto.webappbiblioteca.model.Situacao;



public interface SituacaoService {

	List<Situacao> findAll();

	Situacao findById(Long id);


}
