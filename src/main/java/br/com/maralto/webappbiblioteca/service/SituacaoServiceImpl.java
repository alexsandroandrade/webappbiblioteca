package br.com.maralto.webappbiblioteca.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.maralto.webappbiblioteca.model.Situacao;
import br.com.maralto.webappbiblioteca.repository.SituacaoRepository;



@Service
public class SituacaoServiceImpl implements SituacaoService {

	@Autowired
	SituacaoRepository situacaoRepository;

	@Override
	public List<Situacao> findAll() {
		return situacaoRepository.findAll();
	}

	@Override
	public Situacao findById(Long id) {
		
		return situacaoRepository.findById(id).get();
		
	}
	
	
	


}
